﻿using UnityEngine;
using System;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;
    public bool speedBuff;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100;

    public DateTime speedBuffTimer, newSpeedBuffTimer;

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
        speedBuff = false;
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h, v);

        if (speedBuff)
        {
            SpeedBuff();
        }
    }

    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);

        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }

    void SpeedBuff()
    {
        newSpeedBuffTimer = DateTime.Now;
        TimeSpan speedBuffTimerSpan = newSpeedBuffTimer - speedBuffTimer;
        if (speedBuffTimerSpan.TotalSeconds < 5)
        {
            speed = 15;
        }
        else
        {
            speed = 10;
            speedBuff = false;
        }
    }

}
